#!/usr/bin/env python

import sys
import cv2
import numpy as np

import rclpy
from nav_msgs.msg import OccupancyGrid


class LidarAlarm(rclpy.node.Node):

    def __init__(self):
        super().__init__('lidar_alarm')

        self.declare_parameter('resolution', 0.1)  # meters
        self.declare_parameter('cell_width', 500)  # unit
        self.declare_parameter('cell_height', 3001)  # unit
        self.declare_parameter('safety_x', 3.0)  # lidar x-axis meter default    3.0 from the lidar origin axis-x (forward or rear depend on the moving direction)
        self.declare_parameter('safety_y', 1.0)  # lidar y-axis meter default +- 1.0 from the lidar origin axis-y

        self.resolution = self.get_parameter('resolution').value
        self.cell_width = self.get_parameter('cell_width').value  # unit
        self.cell_height = self.get_parameter('cell_height').value  # unit
        self.safety_x = self.get_parameter('safety_x').value
        self.safety_y = self.get_parameter('safety_y').value
        self.sub = self.create_subscription(OccupancyGrid, '/semantics/costmap_generator/occupancy_grid', self.costmap_callback)

    def costmap_callback(self, msg):
        if msg.info.resolution:
            self.resolution = msg.info.resolution
        if msg.info.width:
            self.cell_width = int(msg.info.width)
        if msg.info.height:
            self.cell_height = int(msg.info.height)
        np_arr = np.array(msg.data).reshape(self.cell_height, self.cell_width)
        # searching grid within safety_width x safety_height
        n_row = int(self.safety_x / self.resolution)  # grid unit
        n_col = int(self.safety_y / self.resolution)  # grid unit
        grid_center_row = int(self.cell_width/2)  # lidar centre in rows
        grid_center_col = int(self.cell_height/2)  # lidar centre in cols

        # TODO: Only look forward for now
        # danger_zone = np_arr[grid_center_row-n_row:grid_center_row+n_row, grid_center_col:grid_center_col+n_col]
        # obstruction = np.where(danger_zone > 0)
        # if(np.any(obstruction)): print("Brake Brake Brake")

        # display data with bounding box added
        img = 2.55 * np_arr
        cv2.rectangle(img, (grid_center_row-n_row, grid_center_col-n_col), (grid_center_row+n_row, grid_center_col+n_col), 255, 2)
        cv2.namedWindow('OCCcostmap', cv2.WINDOW_NORMAL)
        cv2.imshow('OCCcostmap', img)  # remap probability to 255 image scale
        cv2.waitKey(2)


def main(args=None):
    rclpy.init(args=args)

    node = LidarAlarm()
    rclpy.spin(node)

    cv2.destroyAllWindows()
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main(sys.argv)
