from setuptools import setup, find_packages
import xml.etree.ElementTree as ET

root = ET.parse('package.xml').getroot()
package_name = root.findall('name')[-1].text

all_maintainers = root.findall('maintainer')
maintainers = [m.text for m in all_maintainers]
maintainer_emails = [m.attrib['email'] for m in all_maintainers]

all_authors = root.findall('author')
authors = [m.text for m in all_authors]
author_emails = [m.attrib['email'] for m in all_authors]

setup(
    name=package_name,
    version=root.findall('version')[-1].text,
    packages=find_packages(),
    data_files=[('share/' + package_name, ['package.xml'])],
    install_requires=['setuptools'],
    zip_safe=True,
    author=', '.join(authors),
    author_email=', '.join(author_emails),
    maintainer=', '.join(maintainers),
    maintainer_email=', '.join(maintainer_emails),
    keywords=['ROS'],
    description=root.findall('description')[-1].text,
    license=root.findall('license')[-1].text,
    entry_points={'console_scripts': [package_name + ' = ' + package_name + '.' + package_name + ':main']}
)