#!/usr/bin/env python

import rclpy
from std_msgs.msg import Bool
from can_msgs.msg import Frame


class USALarm(rclpy.node.Node):

    def __init__(self):
        super().__init__('us_alarm')

        self.declare_parameter('alarm_topic', '/us/us_alarm')
        self.alarm_topic = self.get_parameter('alarm_topic').value

        self.sub = self.create_subscription(Frame, 'received_messages', self.us_callback)
        self.pub = self.create_publisher(Bool, self.alarm_topic)
        self.frontLeft = 0
        self.frontCentre = 0
        self.frontRight = 0
        self.right = 0
        self.left = 0
        self.rearLeft = 0
        self.rearCentre = 0
        self.rearRight = 0
        self.updated = 0

    def us_callback(self, msg):
        # extract USS CANBus
        if msg.id == 0x402:
            tmp = bytearray(msg.data)
            self.right = tmp[5]
            self.frontRight = tmp[4]
            self.frontCentre = tmp[3]
            self.frontLeft = tmp[2]
            self.updated = 1
        elif msg.id == 0x403:
            tmp = bytearray(msg.data)
            self.left = tmp[5]
            self.rearLeft = tmp[4]
            self.rearCentre = tmp[3]
            self.rearRight = tmp[2]
            self.updated = 1

        # gauging within range: front 4 sensors
        if 0 < self.frontCentre < 255:
            self.pub.publish(True)
            self.get_logger().info("US Alarm: %d %d %d", self.frontLeft, self.frontCentre, self.frontRight)


def main(args=None):
    rclpy.init(args=args)

    node = USALarm()
    rclpy.spin(node)

    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main(sys.argv)