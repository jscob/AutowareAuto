cmake_minimum_required(VERSION 3.5)
project(autoware_auto_msgs)

find_package(ament_cmake REQUIRED)
find_package(rosidl_default_generators REQUIRED)

set(dependencies
        std_msgs
        geometry_msgs
        sensor_msgs
        )

foreach (dependency ${dependencies})
    find_package(${dependency} REQUIRED)
endforeach (dependency)

find_package(ament_cmake REQUIRED)
find_package(rosidl_default_generators REQUIRED)

rosidl_generate_interfaces(${PROJECT_NAME}
        # Helper types
        "msg/Complex32.msg"
        "msg/DiagnosticHeader.msg"
        "msg/TrajectoryPoint.msg"

        # Interfaces
        "msg/ControlDiagnostic.msg"
        "msg/Trajectory.msg"
        "msg/VehicleControlCommand.msg"
        "msg/VehicleKinematicState.msg"
        "msg/VehicleOdometry.msg"
        "msg/VehicleStateCommand.msg"
        "msg/VehicleStateReport.msg"

        # Implementation-specific messages
        "msg/PointClusters.msg"

        DEPENDENCIES ${dependencies})

ament_package()
